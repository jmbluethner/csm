import {getHWID} from 'hwid';
import getMAC, { isMAC } from 'getmac'

function getHwidWithMac() {

    async function main() {
        const id = await getHWID()
        console.log(id);
        return(id+'_'+getMAC())
    }

    main();

}

module.exports = getHwidWithMac;
